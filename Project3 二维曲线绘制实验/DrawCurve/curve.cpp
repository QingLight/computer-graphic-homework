#include "curve.h"
#include <math.h>
#include <GL/glut.h>
double setBezier(int n, int i, float t) {	// n阶贝塞尔曲线绘制的代码
	int sum = 1;
	if (i == n || i == 0) return pow(t, i) * pow(1 - t, n - i);
	int j;
	for (j = n - i + 1; j <= n; j++)
		sum *= j;
	for (j = 1; j <= i; j++)
		sum /= j;
	return sum * pow(t, i) * pow(1 - t, n - i);
}

void bezier(wcPt2D* ctrlPts, int nCtrlPts, wcPt2D* bezCurvePts, int nBezCurvePts) {
	//输入：nCtrlPts个控制点，坐标保存于ctrlPts数组中 
	//输出：nBezCurvePts个曲线上的点，坐标保存于数组bezCurvePts中

	//1.在此处添加代码
	int cnt = 0;
	wcPt2D p_current = ctrlPts[cnt];	// 把当前的控制点设为该段贝塞尔曲线的起点	
	bezCurvePts[cnt] = p_current;	// 把该段贝塞尔曲线的起点存到贝塞尔曲线数组中
	for (float c = 0; c <= 1; c += 1.0 / nBezCurvePts) {	
		wcPt2D p;
		p.x = 0;
		p.y = 0;
		for (int k = 0; k < nCtrlPts; ++k) {	// 计算该段贝塞尔曲线的贝塞尔点
			p.x += ctrlPts[k].x * setBezier(nCtrlPts - 1, k, c);
			p.y += ctrlPts[k].y * setBezier(nCtrlPts - 1, k, c);
		}
		glLineWidth(1.5f);	// 设置线宽
		glBegin(GL_LINE_STRIP);	
		glVertex2f(p_current.x, p_current.y);	// 绘制该段贝塞尔曲线的贝塞尔点
		glVertex2f(p.x, p.y);		// 绘制该段贝塞尔曲线的贝塞尔点
		glEnd();
		p_current = p;
		bezCurvePts[++cnt] = p_current;	// 把该段贝塞尔曲线的起点存到贝塞尔曲线数组中
	}
	// 绘制最后一段的点
	glLineWidth(1.5f);
	glBegin(GL_LINE_STRIP);
	glVertex2f(p_current.x, p_current.y);
	glVertex2f(ctrlPts[nCtrlPts - 1].x, ctrlPts[nCtrlPts - 1].y);
	glEnd();
}
