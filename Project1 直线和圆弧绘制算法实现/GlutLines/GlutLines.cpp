#include <gl/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "math.h"

int defSCREEN_WIDTH = 600, defSCREEN_HEIGHT = 600;
int g_lineNum;
bool g_startSelect,g_endSelect,g_finish;
wcPt2D *g_lines;

void Display();
void Reshape(int w,int h);
void Keyboard(unsigned char uchKey, int iXPos, int iYPos);
void PassiveMotionFunc(int iXPos, int iYPos);
void MyInit(void);
void SetPixel(int x, int y);
void BresLine(int x0,int y0,int x1,int y1,void (*setPixel)(int x,int y));
void MidPLine(int x0,int y0,int x1,int y1,void (*setPixel)(int x,int y));
void MidPoint_Circle(int x0,int y0,int x1,int y1,void (*setPixel)(int x,int y));
void Cirpot(int x0, int y0, int x, int y,void (*setPixel)(int x,int y) );
void swap_value(int* a, int* b);
void OpenGLLine(int x0, int y0, int x1, int y1);

int main(int argc, char *argv[])
{
	glutInit(&argc,argv);
	MyInit();
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowPosition(0,0);
	glutInitWindowSize(defSCREEN_WIDTH,defSCREEN_HEIGHT);

	int windowHandle = glutCreateWindow( "Glut Lines");
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Display);
	glutKeyboardFunc(Keyboard);
	glutPassiveMotionFunc(PassiveMotionFunc);

	glutMainLoop();
	if (g_lines) free(g_lines);
	return 0;
}

void SetPixel(int x, int y)
{
	glEnable(GL_POINT_SMOOTH);
	glBegin(GL_POINTS);
		glVertex2i(x,y);
	glEnd();
}

void Display()  //窗口失效后的回调函数
{
	glClearColor(1.0,1.0,1.0,1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glEnable(GL_LINE_SMOOTH);
	//glLineWidth(5.0);//设置线宽函数
	glPointSize(1.0);//设置点宽函数
	glColor3f(0,0,1.0);//设置点颜色函数
	for(int i=0;i<g_lineNum;i++)
	{
		MidPLine(ROUND(g_lines[i * 2].x), ROUND(g_lines[i * 2].y), ROUND(g_lines[i * 2 + 1].x), ROUND(g_lines[i * 2 + 1].y), SetPixel);
		// BresLine(ROUND(g_lines[i*2].x),ROUND(g_lines[i*2].y),ROUND(g_lines[i*2+1].x),ROUND(g_lines[i*2+1].y),SetPixel);
		// MidPoint_Circle(ROUND(g_lines[i * 2].x), ROUND(g_lines[i * 2].y), ROUND(g_lines[i * 2 + 1].x), ROUND(g_lines[i * 2 + 1].y), SetPixel);
		// OpenGLLine(ROUND(g_lines[i * 2].x), ROUND(g_lines[i * 2].y), ROUND(g_lines[i * 2 + 1].x), ROUND(g_lines[i * 2 + 1].y));
	}	
	glFlush();
}

void Keyboard(unsigned char uchKey, int iXPos, int iYPos)  // 键盘回调函数
{
	if(iXPos<0||iXPos>=defSCREEN_WIDTH||iYPos<0||iYPos>=defSCREEN_HEIGHT)
		return;

	if(uchKey == 27/*Esc*/) {
		exit(0);
	}
	else if(uchKey==' '){
		if(!g_startSelect){

			if(g_lineNum>=40)
			{
				g_finish=true;
				return;
			}

			g_lines[g_lineNum*2].x=g_lines[g_lineNum*2+1].x=iXPos;
			g_lines[g_lineNum*2].y=g_lines[g_lineNum*2+1].y=defSCREEN_HEIGHT - iYPos;

			g_lineNum++;
			g_startSelect=true;
		}
		else{//g_startSelect == true

			g_lines[g_lineNum*2-1].x=iXPos;
			g_lines[g_lineNum*2-1].y=defSCREEN_HEIGHT - iYPos;
			g_startSelect=false;
			g_endSelect=true;
		}
	}
	else if(uchKey=='c'||uchKey=='C'){//清屏
		g_lineNum=0;
		g_startSelect=g_endSelect=g_finish=false;
	}
	glutPostRedisplay();
}
void Reshape(int w,int h) //窗口大小改变后的回调函数
{
	defSCREEN_WIDTH = w;
	defSCREEN_HEIGHT = h;
	glViewport(0.0,0.0,w,h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0,w,0.0,h,1,-1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void MyInit(void)
{
	g_lineNum = 0;
	g_startSelect = g_endSelect = g_finish = false;
	g_lines=(wcPt2D*)malloc(100*sizeof(wcPt2D));
}

void PassiveMotionFunc(int iXPos, int iYPos)//鼠标没有被按下去时,移到鼠标的情形
{
	if(!g_finish){//如果没有完成控制点的选择
		if(g_startSelect){
			g_lines[g_lineNum*2-1].x= iXPos;
			g_lines[g_lineNum*2-1].y= defSCREEN_HEIGHT - iYPos;
			glutPostRedisplay();
		}
		//printf("%d %d\n",iXPos, iYPos);//把两个坐标打印出来
	}
}

void MidPLine(int x0,int y0,int x1,int y1,void (*setPixel)(int x,int y))
{
	//任务1：此处填写代码
	int tag = 0;  // 用于标记计长方向，代表了丨k丨的大小，丨k丨>=1 则 tag=1，反之tag=0
	if (abs(x1 - x0) < abs(y1 - y0)) {
		swap(x0, y0);
		swap(x1, y1);  // 此情况下以x1,x2表示y轴的点;y1,y2表示x轴的点，以y轴为计长方向
		tag = 1;	   // 此情况下丨k丨>=1 则 tag=1
	}
	if (x0 > x1)
	{					// 确保以x0<=x1,以方便后面的运算
		swap(x0, x1);  
		swap(y0, y1);
	}
	int a = y0 - y1, b = x1 - x0, d = a + b / 2;  // d用于下一个像素位置的判别式
	int x, y;
	if (y0 < y1) {
		x = x0, y = y0;
		setPixel(x, y);
		while (x < x1) {
			if (d < 0) {
				x++; y++;
				d = d + a + b;  // 取直线上方的像素点
			}
			else {
				x++;
				d += a;         // 取直线下方的像素点
			}
			if (tag) setPixel(y, x);
			else setPixel(x, y);
		}
	}
	else {
		x = x1, y = y1;
		setPixel(x, y);
		while (x > x0) {
			if (d < 0) {
				x--; y++;
				d = d - a + b;  // 取直线上方的像素点
			}
			else {
				x--;
				d -= a;         // 取直线下方的像素点
			}
			if (tag) setPixel(y, x);
			else setPixel(x, y);
		}
	}
}

void swap_value(int* a, int* b) {	// 此函数用于值交换
	int temp = *a;
	*a = *b;
	*b = temp;
}

void BresLine(int x0, int y0, int x1, int y1, void (*setPixel)(int x, int y)) {
	SetPixel(x0, y0);		// 绘出起始点(x0,y0)
	int dx = abs(x0 - x1);	// 表示两点x轴方向上的差值
	int dy = abs(y0 - y1);	// 表示两点y轴方向上的差值
	if (dx == 0 && dy == 0) // 当(x0,y0)(x1,y1)表示同一个点时，退出函数。
		return;
	int flag = 0;			// 用于标记计长方向，代表了丨k丨的大小，丨k丨>=1 则 flag=1，反之flag=0
	if (dx < dy){	// 下面将斜率变换至0≤丨k丨≤1区间
		flag = 1;	// 此情况下丨k丨>=1，以x1,x2表示y轴的点;y1,y2表示x轴的点，以y轴为计长方向
		swap_value(&x0, &y0);	
		swap_value(&x1, &y1);
		swap_value(&dx, &dy);
	}
	int tx = (x1 - x0) > 0 ? 1 : -1;	// tx表示直线相对于起点在x轴的方向
	int ty = (y1 - y0) > 0 ? 1 : -1;	// ty表示直线相对于起点在y轴的方向
	int curx = x0;
	int cury = y0;
	int dS = 2 * dy;			// 用于表示迭代式中的项
	int dT = 2 * (dy - dx);		// 用于表示迭代式中的项
	int d = dS - dx;			// 用于表示迭代式中的项
	while (curx != x1){
		if (d < 0)				// 取直线下方的像素点
			d += dS;
		else{					// 取直线上方的像素点
			cury += ty; 
			d += dT;
		}
		if(flag)
			SetPixel(cury, curx);
		else
			SetPixel(curx, cury);
		curx += tx;
	}
}

//绘制以（x0,y0）为中心，以（x1,y1）到(x0,y0)的距离为半径的圆
void MidPoint_Circle(int x0,int y0,int x1,int y1,void (*setPixel)(int x,int y))
{
	//任务3：此处填写代码，PPT有算法，照片
	int r = sqrt((1.0 * x1 - x0) * (1.0 * x1 - x0) + (1.0 * y1 - y0) * (1.0 * y1 - y0));	// 计算半径
	int x = 0;
	int y = r;
	int d = 1 - r;	// 是公式中 1.25-r 取整后的结果
	Cirpot(x0, y0, x, y, SetPixel);	// 进行8路对称
	while (x < y)
	{
		if (d < 0)
			d += 2 * x + 3;		// 表明当前中点在圆内，下一个点亮的像素点选取圆外的像素点
		else {
			d += 2 * (x - y) + 5;	// 表明当前中点在圆外，下一个点亮的像素点选取圆内的像素点
			y--;
		}
		x++;
		Cirpot(x0, y0, x, y, SetPixel);
	}
}

void Cirpot(int x0, int y0, int x, int y,void (*setPixel)(int x,int y) )
{	// 8路对称
	setPixel((x0 + x), (y0 + y));	// 绘出第一象限中离x轴较近的点
	setPixel((x0 + y), (y0 + x));	// 绘出第一象限中离y轴较近的点
	setPixel((x0 + y), (y0 - x));	// 绘出第四象限中离y轴较近的点
	setPixel((x0 + x), (y0 - y));	// 绘出第四象限中离x轴较近的点
	setPixel((x0 - x), (y0 - y));	// 绘出第三象限中离x轴较近的点
	setPixel((x0 - y), (y0 - x));	// 绘出第三象限中离y轴较近的点
	setPixel((x0 - y), (y0 + x));	// 绘出第二象限中离y轴较近的点
	setPixel((x0 - x), (y0 + y));	// 绘出第二象限中离x轴较近的点
}

void OpenGLLine(int x0,int y0,int x1,int y1)
{
	//任务4：此处填写代码,照片，PPT
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f); glVertex2f(x0, y0);	// 设定起点坐标与起始颜色
	glColor3f(0.0f, 1.0f, 0.0f); glVertex2f(x1, y1);	// 设定终点坐标与终点颜色
	glEnd();
}